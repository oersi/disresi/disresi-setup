import base64
from datetime import datetime
import io
import logging
import re
import requests
import getopt, os, sys
from PIL import Image, ImageOps
import cairosvg

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.{{ thumbnail_creation_loglevel }})
metadata_search_api_url = "{{ search_index_backend_searchapi_url }}{{ elasticsearch_metadata_index_alias_name }}/_search"
thumbnail_webserver_path = "{{ thumbnail_webserver_instdir }}/"
image_source_field = "{{ thumbnail_image_source_field }}"
image_width = {{thumbnail_image_width}}
image_height = {{thumbnail_image_height}}
image_creation_method = "{{ thumbnail_creation_method }}"
splash_base_url = "{{ thumbnail_splash_base_url }}"
image_output_format = "webp"  # | "PNG" | "JPEG"
pad_image_url_regexes = {{thumbnail_creation_pad_images_regexes | to_json | trim}}
max_filename_length = 255
user_agent = "{{ application_name }}-ThumbnailLoaderBot/0.1 ({{ search_index_host }}, {{ search_index_public_base_url }}{{ search_index_public_base_path }}/services/contact) {{ application_name }}-Import/0.1"


class SearchIndexDataLoader:
    def __init__(self):
        self.image_urls_loaded = False
        self.image_urls_after_key = None
        self.ids_without_image_loaded = False
        self.ids_without_image_after_key = None

    def load_next_image_urls(self):
        if self.image_urls_loaded:
            return None
        headers = {'User-Agent': user_agent, 'Content-type': 'application/json', 'Accept': 'application/json'}
        data = {
            "size": 0,
            "aggregations": {
                "images": {
                    "composite": {
                        "size": 2000,
                        "sources": [
                            {"keyword": {"terms": {"field": image_source_field}}}
                        ]
                    },
                    "aggregations": {
                        "resource_identifiers": {
                            "terms": {
                                "size": 5000,
                                "field": "_id"
                            }
                        }
                    }
                }
            }
        }
        if self.image_urls_after_key:
            data["aggregations"]["images"]["composite"]["after"] = self.image_urls_after_key

        result = requests.post(metadata_search_api_url, headers=headers, json=data)
        json_result = result.json()

        if "after_key" in json_result["aggregations"]["images"]:
            self.image_urls_after_key = json_result["aggregations"]["images"]["after_key"]
        else:
            self.image_urls_loaded = True

        return list(
            filter(
                None,
                [{"image_url": b["key"]["keyword"], "resource_identifiers": [a["key"] for a in b["resource_identifiers"]["buckets"]]} for b in json_result["aggregations"]["images"]["buckets"]]
            )
        )

    def load_next_ids_without_image(self):
        if self.ids_without_image_loaded:
            return None
        headers = {'User-Agent': user_agent, 'Content-type': 'application/json', 'Accept': 'application/json'}
        data = {
            "size": 0,
            "query": {
                "bool": {
                    "must_not": {
                        "exists": {"field": image_source_field}
                    }
                }
            },
            "aggregations": {
                "resource_identifiers": {
                    "composite": {
                        "size": 2000,
                        "sources": [
                            {"keyword": {"terms": {"field": "_id"}}}
                        ]
                    }
                }
            }
        }
        if self.ids_without_image_after_key:
            data["aggregations"]["resource_identifiers"]["composite"]["after"] = self.ids_without_image_after_key

        result = requests.post(metadata_search_api_url, headers=headers, json=data)
        json_result = result.json()

        if "after_key" in json_result["aggregations"]["resource_identifiers"]:
            self.ids_without_image_after_key = json_result["aggregations"]["resource_identifiers"]["after_key"]
        else:
            self.ids_without_image_loaded = True

        return list(
            filter(
                None,
                [b["key"]["keyword"] for b in json_result["aggregations"]["resource_identifiers"]["buckets"]]
            )
        )


class SearchIndexThumbnailCreator:
    def __init__(self, skip_for_existing_files):
        self.skip_for_existing_files = skip_for_existing_files
        self.method = {
            "NEAREST": Image.Resampling.NEAREST,
            "BILINEAR": Image.Resampling.BILINEAR,
            "BICUBIC": Image.Resampling.BICUBIC,
            "LANCZOS": Image.Resampling.LANCZOS
        }.get(image_creation_method)

    def __convert_image_url_to_thumbnail__(self, image_url, image_url_params, base64_ids):
        extension = "." + image_output_format.lower()
        outfilenames = [file_id[:max_filename_length - len(extension)] if len(file_id) > max_filename_length - len(extension) else file_id for file_id in base64_ids]
        outfiles = [thumbnail_webserver_path + filename + extension for filename in outfilenames]
        if self.skip_for_existing_files:
            for outfile in outfiles:
                if os.path.isfile(outfile):
                    logging.debug("Skipping existing file %s", outfile)
            outfiles = [f for f in outfiles if not os.path.isfile(f)]
            if not outfiles:
                return
        image_response = requests.get(image_url, params=image_url_params, headers={'User-Agent': user_agent})
        image_bytes = io.BytesIO(image_response.content)
        if "Content-Type" in image_response.headers and image_response.headers["Content-Type"] == "image/svg+xml":
            logging.debug("Received svg -> convert")
            image_bytes = io.BytesIO(cairosvg.svg2png(file_obj=image_bytes, scale=10))
        with Image.open(image_bytes) as im:
            for pad_regex in pad_image_url_regexes:
                if re.match(pad_regex, image_url):
                    im = im.convert(mode="RGBA")  # some images cannot be processed correctly, if not converted to mode RGB first, because most ImageOps-operators only work on L and RGB
                    im = ImageOps.pad(im, (image_width, image_height))
            thumbnail = ImageOps.fit(im, (image_width, image_height), self.method)
            for outfile in outfiles:
                thumbnail.save(outfile, image_output_format)
                logging.debug("Created " + outfile)

    def convert_source_image_to_thumbnail(self, image_data):
        try:
            self.__convert_image_url_to_thumbnail__(image_data["image_url"], {}, image_data["resource_identifiers"])
        except Exception as e:
            logging.warning(e)
            logging.warning("cannot convert to thumbnail for %s", image_data["image_url"])
            for identifier in image_data["resource_identifiers"]:
                self.create_splash_thumbnail_for_base64_resource_id(identifier)

    def create_splash_thumbnail_for_base64_resource_id(self, base64_id):
        url = base64.urlsafe_b64decode(base64_id.encode()).decode("utf-8")
        logging.info("Creating splash thumbnail for %s (%s)", url, base64_id)
        # https://splash.readthedocs.io/en/stable/api.html#render-png
        splash_image_url = splash_base_url + "/render.png"
        splash_params = {"engine": "chromium", "url": url, "width": image_width, "height": image_height, "wait": "0.7"}
        try:
            self.__convert_image_url_to_thumbnail__(splash_image_url, splash_params, [base64_id])
        except Exception as e:
            logging.warning(e)
            logging.warning("cannot create splash thumbnail for %s", url)


def generate_thumbnails_for_resources_without_image(skip_existing):
    metadata_loader = SearchIndexDataLoader()
    thumbnail_creator = SearchIndexThumbnailCreator(skip_existing)
    index_base64_ids_without_image = metadata_loader.load_next_ids_without_image()
    while index_base64_ids_without_image is not None:
        logging.debug("Processing next %s ids without image", str(len(index_base64_ids_without_image)))
        for base64_id in index_base64_ids_without_image:
            thumbnail_creator.create_splash_thumbnail_for_base64_resource_id(base64_id)
        index_base64_ids_without_image = metadata_loader.load_next_ids_without_image()


def generate_thumbnails_for_resource_images(skip_existing):
    metadata_loader = SearchIndexDataLoader()
    thumbnail_creator = SearchIndexThumbnailCreator(skip_existing)
    next_index_image_data = metadata_loader.load_next_image_urls()
    while next_index_image_data is not None:
        logging.debug("Processing next %s image urls", str(len(next_index_image_data)))
        for image_data in next_index_image_data:
            thumbnail_creator.convert_source_image_to_thumbnail(image_data)
        next_index_image_data = metadata_loader.load_next_image_urls()


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s", ["skip-existing"])
    except getopt.GetoptError as err:
        logging.error(err)
        sys.exit(2)
    skip_existing = False
    for o, a in opts:
        if o in ("-s", "--skip-existing"):
            skip_existing = True

    start = datetime.today()
    logging.info("Start")
    logging.info("skip thumbnail-creation for already existing thumbnails: %s", str(skip_existing))

    generate_thumbnails_for_resources_without_image(skip_existing)
    generate_thumbnails_for_resource_images(skip_existing)

    end = datetime.today()
    logging.info("Done (duration: %s)", str(end - start))
