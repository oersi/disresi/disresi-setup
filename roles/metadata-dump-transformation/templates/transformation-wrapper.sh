#!/bin/bash
# {{ ansible_managed }}
#
# Wrapper script to create a metadata dump transformation
#

WORK_DIR={{ base_dir }}/.dump
TARGET_DIR={{ search_index_dumps_instdir }}
DUMP_FILE="${WORK_DIR}/{{ elasticsearch_metadata_index_alias_name }}.{{ dump_transformation_output_file_extension }}"

date +"*** %Y-%m-%d %H:%M:%S Starting dump transformation in $WORK_DIR"
mkdir -p $WORK_DIR

cd {{ dump_transformation_instdir }} && ./transform.sh {{ search_index_dumps_instdir }}/{{ elasticsearch_metadata_index_alias_name }}.ndjson $DUMP_FILE {{ search_index_public_base_url }}{{ search_index_public_base_path }} $TARGET_DIR

date +"*** %Y-%m-%d %H:%M:%S finalizing dump file"
gzip -fk $DUMP_FILE
cp ${DUMP_FILE} $TARGET_DIR
cp ${DUMP_FILE}.gz $TARGET_DIR
rm ${DUMP_FILE}
rm ${DUMP_FILE}.gz

date +"*** %Y-%m-%d %H:%M:%S done"
