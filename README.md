# Setup of Search Index for Distributed Repositories

The search index offers the possibility to search quickly in various sources. For this purpose, the metadata of the sources is retrieved regularly.

With this project you can set up all components that are necessary to run the index. The process uses [ansible](https://docs.ansible.com/) to install the components.

This repo is intended as a basis for a standard installation of a specific search index, such as OERSI or RESODATE. It contains the Ansible scripts required for installation and update. The individual features are set up in the setup repos of the specific search indexes via the configuration, see for example https://gitlab.com/oersi/oersi-setup.

**NOTE**: The Ansible scripts were originally developed for the OERSI, but can now also be used in various other search indexes.

A more detailed documentation can be found here: https://oersi.gitlab.io/sidre/sidre-docs/

## Example

An example configuration for a specific search index repo can be found in [setup-config-example](doc/setup-config-example)

## Contributing

Contributions are welcome. Please read [CONTRIBUTING.md](CONTRIBUTING.md) for further details.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
