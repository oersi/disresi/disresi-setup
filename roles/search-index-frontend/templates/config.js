window['runTimeConfig'] = {
  BACKEND_API: {
    BASE_URL: "{{ search_index_public_base_url }}",
    PATH_CONTACT: "{{ search_index_backend_contactapi_root_path }}",
    PATH_LABEL: "{{ search_index_backend_labelapi_root_path }}",
    PATH_SEARCH: "{{ search_index_backend_searchapi_root_path }}"
  },
  ELASTIC_SEARCH_INDEX_NAME: "{{ search_index_frontend_index_name }}",
  GENERAL_CONFIGURATION: {
    AVAILABLE_LANGUAGES: {{ search_index_frontend_available_languages  | default([], true) | to_json(ensure_ascii=False) }},
    PUBLIC_URL: "{{ search_index_public_base_url }}{{ search_index_public_base_path }}",
    RESULT_PAGE_SIZE_OPTIONS: {{ search_index_frontend_page_size  | default([], true) | to_json(ensure_ascii=False) }},
    NR_OF_RESULT_PER_PAGE: {{search_index_frontend_nr_result_page_default}},
    HEADER_LOGO_URL: "{{ search_index_frontend_header_logo_url }}",
    DEFAULT_SOCIAL_MEDIA_IMAGE: "{{ search_index_frontend_html_meta_image | default('null', true) }}",
    THEME_COLORS: {{ search_index_frontend_theme_colors | default('null', true) }},
    THEME_COLORS_DARK: {{ search_index_frontend_theme_colors_dark | default('null', true) }},
    PRIVACY_POLICY_LINK: {{search_index_frontend_custom_cookie_links | default([], true) | to_json(ensure_ascii=False,indent=0) }},
    EXTERNAL_INFO_LINK: {{search_index_frontend_custom_info_links| default({}, true) | to_json(ensure_ascii=False,indent=0) }},
    I18N_CACHE_EXPIRATION: {{ search_index_frontend_i18n_cache_expiration }},
    I18N_DEBUG: {{ search_index_frontend_i18n_debug }},
    TRACK_TOTAL_HITS: {{ search_index_frontend_track_total_hits }},
    FEATURES: {
      DARK_MODE: {{ search_index_frontend_features_dark_mode }},
      CHANGE_FONTSIZE: {{ search_index_frontend_features_change_font_size }},
      RESOURCE_EMBEDDING_SNIPPET: {{ search_index_frontend_features_resource_embedding_snippet }},
      SIDRE_THUMBNAILS: {{ search_index_frontend_features_sidre_thumbnails }},
      SCROLL_TOP_BUTTON: {{ search_index_frontend_features_scroll_top_button }},
    },
    fieldConfiguration: {{ search_index_frontend_field_configuration | default({}, true) | to_json(ensure_ascii=False) }},
    search: {{ search_index_frontend_search_configuration | default({}, true) | to_json(ensure_ascii=False) }},
    resultCard: {{ search_index_frontend_result_card_configuration | default({}, true) | to_json(ensure_ascii=False) }},
    detailPage: {{ search_index_frontend_detail_page_configuration | default({}, true) | to_json(ensure_ascii=False) }},
    embeddedStructuredDataAdjustments: {{ search_index_frontend_embedded_structured_data_adjustments | default([], true) | to_json(ensure_ascii=False) }}
  }
}
